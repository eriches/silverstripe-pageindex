<?php

namespace Hestec\PageIndex;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\GridField\GridFieldPaginator;
use SilverStripe\Forms\GridField\GridFieldPageCount;

class PageIndexExtension extends DataExtension {

    private static $has_many = array(
        'Chapters' => Chapter::class
    );

    public function updateCMSFields(FieldList $fields) {

        $copycode = '';

        foreach ($this->owner->Chapters() as $node){

            $copycode .= $node->Anchor."\n";

        }
        $copycode .= "\n";
        foreach ($this->owner->Chapters() as $node){

            $copycode .= "class=\"anchor\" id=\"".$node->Anchor."\"\n";

        }

        $CopyCodeField = TextareaField::create('CopyCode', _t('ElementIndex.COPYCODE', "Code for copy"), $copycode);
        $CopyCodeField->setDescription(_t('ElementIndex.COPYCODE_DESCRIPION', "You can copy and paste this code into a notepad file and use it in the source code of the page."));

        if ($this->owner->ID) {

            $ChaptersGridField = GridField::create(
                'Chapters',
                _t('ElementIndex.CHAPTERS', "Chapters"),
                $this->owner->Chapters(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
                    ->removeComponentsByType(GridFieldPaginator::class)
                    ->removeComponentsByType(GridFieldPageCount::class)
            );

            //$field = TextField::create('test', 'test');
            $fields->addFieldToTab('Root.Index', $ChaptersGridField);
            $fields->addFieldToTab('Root.Index', $CopyCodeField);
        }

    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        //$fields->removeByName('Chapters');

        $copycode = '';

        foreach ($this->Chapters() as $node){

            $copycode .= $node->Anchor."\n";

        }
        $copycode .= "\n";
        foreach ($this->Chapters() as $node){

            $copycode .= "class=\"anchor\" id=\"".$node->Anchor."\"\n";

        }

        $CopyCodeField = TextareaField::create('CopyCode', _t('ElementIndex.COPYCODE', "Code for copy"), $copycode);
        $CopyCodeField->setDescription(_t('ElementIndex.COPYCODE_DESCRIPION', "You can copy and paste this code into a notepad file and use it in the source code of the page."));

        if ($this->ID) {

            $ChaptersGridField = GridField::create(
                'Chapters',
                _t('ElementIndex.CHAPTERS', "Chapters"),
                $this->Chapters(),
                GridFieldConfig_RecordEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
            );

            //$field = TextField::create('test', 'test');
            $fields->addFieldToTab('Root.Main', $ChaptersGridField);
            $fields->addFieldToTab('Root.Main', $CopyCodeField);
        }
        return $fields;
    }

}
